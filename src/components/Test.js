import React from 'react'

export class Test extends React.Component {

  state = {
    showSpinner: false,
    listPokemon: []
  }

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.getPokemon()
  }
  
  async getPokemon() {
    const response = await fetch('https://pokeapi.co/api/v2/pokemon').then((response) => response.json())
    this.setState({
      listPokemon: response.results
    })
  }

  test = () => {
    this.setState({
      showSpinner: !this.state.showSpinner
    })
  }

  render() {
    const { propTest, children, testMethod } = this.props

    return (
      <div>
        {
          this.state.showSpinner ? <span>Loading...</span> : <span> {propTest} </span>
        }
        {children}
        <button onClick={this.test}>Boton</button>
        <button onClick={() => {
          testMethod('wakala')
        }}>Boton 2</button>

        {
          this.state.listPokemon.map((p,i) => {
            return (
              <div key={`pk-${i}`} > {p.name} </div>
            )
          })
        }
      </div>
    )
  }
}