import React, {useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import { Test } from './components/Test';

function App() {

  useEffect(() => {
    
  })

  const test2 = (str) => {
    console.log('str:', str)
  }

  return (
    <div className="App">
      <Test testMethod={test2} propTest="hola prro">
        <span>Felipe Mamahuevo</span>
      </Test>
    </div>
  );
}

export default App;
